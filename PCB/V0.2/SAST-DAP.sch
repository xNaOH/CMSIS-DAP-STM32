EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:USB_C_Plug_USB2.0 P1
U 1 1 5EBA7FC9
P 1050 1500
F 0 "P1" H 1157 2367 50  0000 C CNN
F 1 "USB_C_Plug_USB2.0" H 1157 2276 50  0000 C CNN
F 2 "Connector_USB:USB_C_Receptacle_HRO_TYPE-C-31-M-12" H 1200 1500 50  0001 C CNN
F 3 "https://www.usb.org/sites/default/files/documents/usb_type-c.zip" H 1200 1500 50  0001 C CNN
	1    1050 1500
	1    0    0    -1  
$EndComp
NoConn ~ 1650 1100
NoConn ~ 1650 1200
$Comp
L Device:R R1
U 1 1 5EBA9BC7
P 1900 1400
F 0 "R1" V 1900 1400 50  0000 C CNN
F 1 "220" V 2000 1400 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1830 1400 50  0001 C CNN
F 3 "~" H 1900 1400 50  0001 C CNN
	1    1900 1400
	0    1    1    0   
$EndComp
$Comp
L Device:R R2
U 1 1 5EBAA84A
P 1900 1600
F 0 "R2" V 1900 1600 50  0000 C CNN
F 1 "220" V 2000 1600 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1830 1600 50  0001 C CNN
F 3 "~" H 1900 1600 50  0001 C CNN
	1    1900 1600
	0    1    1    0   
$EndComp
$Comp
L Device:R R3
U 1 1 5EBAABA1
P 2150 1850
F 0 "R3" H 2080 1804 50  0000 R CNN
F 1 "152" H 2080 1895 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2080 1850 50  0001 C CNN
F 3 "~" H 2150 1850 50  0001 C CNN
	1    2150 1850
	-1   0    0    1   
$EndComp
Text GLabel 2250 1400 2    50   Input ~ 0
USB_N
Wire Wire Line
	1750 1400 1650 1400
Wire Wire Line
	1650 1600 1750 1600
Text GLabel 2250 1600 2    50   Input ~ 0
USB_P
Wire Wire Line
	2050 1400 2250 1400
Wire Wire Line
	2050 1600 2150 1600
Wire Wire Line
	2150 1700 2150 1600
Connection ~ 2150 1600
Wire Wire Line
	2150 1600 2250 1600
$Comp
L power:GND #PWR01
U 1 1 5EBADE82
P 1050 2400
F 0 "#PWR01" H 1050 2150 50  0001 C CNN
F 1 "GND" H 1055 2227 50  0000 C CNN
F 2 "" H 1050 2400 50  0001 C CNN
F 3 "" H 1050 2400 50  0001 C CNN
	1    1050 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 2400 750  2400
Connection ~ 1050 2400
$Comp
L power:+3.3V #PWR02
U 1 1 5EBAEF96
P 2150 2100
F 0 "#PWR02" H 2150 1950 50  0001 C CNN
F 1 "+3.3V" H 2165 2273 50  0000 C CNN
F 2 "" H 2150 2100 50  0001 C CNN
F 3 "" H 2150 2100 50  0001 C CNN
	1    2150 2100
	-1   0    0    1   
$EndComp
Wire Wire Line
	2150 2000 2150 2100
$Comp
L Device:Fuse F1
U 1 1 5EBAFCB6
P 1900 900
F 0 "F1" V 1703 900 50  0000 C CNN
F 1 "Fuse" V 1794 900 50  0000 C CNN
F 2 "Fuse:Fuse_0805_2012Metric" V 1830 900 50  0001 C CNN
F 3 "~" H 1900 900 50  0001 C CNN
	1    1900 900 
	0    1    1    0   
$EndComp
Wire Wire Line
	1750 900  1650 900 
Wire Wire Line
	2050 900  2650 900 
Wire Notes Line
	450  2700 7550 2700
Connection ~ 2650 900 
Wire Wire Line
	2000 5650 2100 5650
Wire Wire Line
	2000 5250 2100 5250
NoConn ~ 3400 4450
NoConn ~ 3400 4550
NoConn ~ 3400 4750
NoConn ~ 3400 4950
NoConn ~ 3400 5150
NoConn ~ 3400 5250
NoConn ~ 3400 5750
NoConn ~ 3400 5850
NoConn ~ 3400 5950
NoConn ~ 2100 5950
NoConn ~ 2100 5850
NoConn ~ 2100 5750
NoConn ~ 2100 5550
NoConn ~ 2100 5450
NoConn ~ 2100 5350
NoConn ~ 2100 5150
NoConn ~ 2100 5050
NoConn ~ 2100 4950
NoConn ~ 2100 4850
NoConn ~ 2100 4750
NoConn ~ 2100 4550
NoConn ~ 2100 4450
NoConn ~ 2100 4250
NoConn ~ 2100 4150
NoConn ~ 2100 4050
Wire Wire Line
	4450 3450 4350 3450
Wire Wire Line
	4350 3350 4450 3350
$Comp
L Device:R R10
U 1 1 5ECEF6FD
P 4600 3450
F 0 "R10" V 4600 3450 50  0000 C CNN
F 1 "220" V 4650 3600 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4530 3450 50  0001 C CNN
F 3 "~" H 4600 3450 50  0001 C CNN
	1    4600 3450
	0    1    1    0   
$EndComp
$Comp
L Device:R R9
U 1 1 5ECED1BE
P 4600 3350
F 0 "R9" V 4600 3350 50  0000 C CNN
F 1 "220" V 4650 3500 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4530 3350 50  0001 C CNN
F 3 "~" H 4600 3350 50  0001 C CNN
	1    4600 3350
	0    1    1    0   
$EndComp
$Comp
L Device:C C6
U 1 1 5EC4546C
P 4550 4200
F 0 "C6" H 4665 4246 50  0000 L CNN
F 1 "104" H 4665 4155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4588 4050 50  0001 C CNN
F 3 "~" H 4550 4200 50  0001 C CNN
	1    4550 4200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C7
U 1 1 5EC468AC
P 4900 4200
F 0 "C7" H 5015 4246 50  0000 L CNN
F 1 "104" H 5015 4155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4938 4050 50  0001 C CNN
F 3 "~" H 4900 4200 50  0001 C CNN
	1    4900 4200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C8
U 1 1 5EC47C5F
P 5250 4200
F 0 "C8" H 5365 4246 50  0000 L CNN
F 1 "104" H 5365 4155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5288 4050 50  0001 C CNN
F 3 "~" H 5250 4200 50  0001 C CNN
	1    5250 4200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C9
U 1 1 5EC4916A
P 5600 4200
F 0 "C9" H 5715 4246 50  0000 L CNN
F 1 "104" H 5715 4155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5638 4050 50  0001 C CNN
F 3 "~" H 5600 4200 50  0001 C CNN
	1    5600 4200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR09
U 1 1 5EC4A620
P 5100 4400
F 0 "#PWR09" H 5100 4150 50  0001 C CNN
F 1 "GND" H 5105 4227 50  0000 C CNN
F 2 "" H 5100 4400 50  0001 C CNN
F 3 "" H 5100 4400 50  0001 C CNN
	1    5100 4400
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR08
U 1 1 5EC4AF1D
P 5100 4000
F 0 "#PWR08" H 5100 3850 50  0001 C CNN
F 1 "+3.3V" H 5115 4173 50  0000 C CNN
F 2 "" H 5100 4000 50  0001 C CNN
F 3 "" H 5100 4000 50  0001 C CNN
	1    5100 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 4050 4550 4000
Wire Wire Line
	4550 4000 4900 4000
Wire Wire Line
	5600 4000 5600 4050
Connection ~ 5100 4000
Wire Wire Line
	5100 4000 5250 4000
Wire Wire Line
	5250 4050 5250 4000
Connection ~ 5250 4000
Wire Wire Line
	5250 4000 5600 4000
Wire Wire Line
	4900 4050 4900 4000
Connection ~ 4900 4000
Wire Wire Line
	4900 4000 5100 4000
Wire Wire Line
	4900 4350 4900 4400
Wire Wire Line
	4900 4400 5100 4400
Wire Wire Line
	5250 4350 5250 4400
Wire Wire Line
	5250 4400 5100 4400
Connection ~ 5100 4400
Wire Wire Line
	5600 4350 5600 4400
Wire Wire Line
	5600 4400 5250 4400
Connection ~ 5250 4400
Wire Wire Line
	4550 4350 4550 4400
Wire Wire Line
	4550 4400 4900 4400
Connection ~ 4900 4400
$Comp
L Connector_Generic:Conn_02x04_Odd_Even J1
U 1 1 5EC8347A
P 5050 3250
F 0 "J1" H 5100 3567 50  0000 C CNN
F 1 "Conn_02x04_Odd_Even" H 5100 3476 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x04_P2.54mm_Horizontal" H 5050 3250 50  0001 C CNN
F 3 "~" H 5050 3250 50  0001 C CNN
	1    5050 3250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR018
U 1 1 5EC87201
P 4750 3250
F 0 "#PWR018" H 4750 3000 50  0001 C CNN
F 1 "GND" V 4755 3122 50  0000 R CNN
F 2 "" H 4750 3250 50  0001 C CNN
F 3 "" H 4750 3250 50  0001 C CNN
	1    4750 3250
	0    1    1    0   
$EndComp
Text GLabel 4350 3450 0    50   Input ~ 0
TMS
Text GLabel 4350 3350 0    50   Input ~ 0
TCK
Wire Wire Line
	4750 3350 4850 3350
Wire Wire Line
	4850 3450 4750 3450
Text GLabel 5450 3350 2    50   Input ~ 0
TX
Text GLabel 5450 3450 2    50   Input ~ 0
RX
$Comp
L power:GND #PWR019
U 1 1 5EC9EA0F
P 5450 3250
F 0 "#PWR019" H 5450 3000 50  0001 C CNN
F 1 "GND" V 5455 3122 50  0000 R CNN
F 2 "" H 5450 3250 50  0001 C CNN
F 3 "" H 5450 3250 50  0001 C CNN
	1    5450 3250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5350 3350 5450 3350
Wire Wire Line
	5450 3450 5350 3450
Wire Notes Line
	7550 2700 7550 450 
Wire Wire Line
	5450 3250 5350 3250
Wire Wire Line
	5350 3150 5450 3150
Text GLabel 5450 3150 2    50   Input ~ 0
RST
Wire Wire Line
	4850 3250 4750 3250
Wire Wire Line
	4750 3150 4850 3150
$Comp
L power:+3.3V #PWR017
U 1 1 5EC87A1B
P 4750 3150
F 0 "#PWR017" H 4750 3000 50  0001 C CNN
F 1 "+3.3V" V 4765 3278 50  0000 L CNN
F 2 "" H 4750 3150 50  0001 C CNN
F 3 "" H 4750 3150 50  0001 C CNN
	1    4750 3150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1600 4000 2050 4000
Connection ~ 1600 4000
Wire Wire Line
	1600 3700 2050 3700
Connection ~ 1600 3700
$Comp
L Device:Crystal Y1
U 1 1 5EC823C8
P 1600 3850
F 0 "Y1" V 1554 3981 50  0000 L CNN
F 1 "Crystal" V 1645 3981 50  0000 L CNN
F 2 "Crystal:Crystal_SMD_5032-2Pin_5.0x3.2mm" H 1600 3850 50  0001 C CNN
F 3 "~" H 1600 3850 50  0001 C CNN
	1    1600 3850
	0    1    1    0   
$EndComp
Wire Wire Line
	1500 4000 1600 4000
Wire Wire Line
	1500 3700 1600 3700
Wire Notes Line
	6500 2700 6500 450 
Wire Notes Line
	5200 2700 5200 450 
Wire Wire Line
	6950 950  7050 950 
Wire Wire Line
	6950 1050 6950 950 
Text GLabel 7050 950  2    50   Input ~ 0
BOOT0
Wire Wire Line
	6950 1450 6950 1350
Wire Wire Line
	6850 1950 6850 1850
Wire Wire Line
	7050 1850 7050 1950
$Comp
L Device:R R8
U 1 1 5EC5EA37
P 6950 1200
F 0 "R8" H 7020 1246 50  0000 L CNN
F 1 "103" H 7020 1155 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6880 1200 50  0001 C CNN
F 3 "~" H 6950 1200 50  0001 C CNN
	1    6950 1200
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR016
U 1 1 5EC5E014
P 7050 1950
F 0 "#PWR016" H 7050 1700 50  0001 C CNN
F 1 "GND" V 7055 1822 50  0000 R CNN
F 2 "" H 7050 1950 50  0001 C CNN
F 3 "" H 7050 1950 50  0001 C CNN
	1    7050 1950
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR015
U 1 1 5EC5CAD3
P 6850 1950
F 0 "#PWR015" H 6850 1800 50  0001 C CNN
F 1 "+3.3V" V 6865 2078 50  0000 L CNN
F 2 "" H 6850 1950 50  0001 C CNN
F 3 "" H 6850 1950 50  0001 C CNN
	1    6850 1950
	-1   0    0    1   
$EndComp
$Comp
L Switch:SW_SPDT SW1
U 1 1 5EC5BBFF
P 6950 1650
F 0 "SW1" V 6904 1798 50  0000 L CNN
F 1 "SW_SPDT" V 6995 1798 50  0000 L CNN
F 2 "Button_Switch_SMD:SW_SPDT_PCM12" H 6950 1650 50  0001 C CNN
F 3 "~" H 6950 1650 50  0001 C CNN
	1    6950 1650
	0    1    1    0   
$EndComp
Wire Wire Line
	6150 1050 6150 1150
Wire Wire Line
	6050 1050 6150 1050
Wire Wire Line
	5650 1050 5650 1150
Wire Wire Line
	5550 1050 5650 1050
Text GLabel 6050 1050 0    50   Input ~ 0
LED2
Text GLabel 5550 1050 0    50   Input ~ 0
LED1
Wire Wire Line
	6150 1450 6150 1550
Wire Wire Line
	5650 1450 5650 1550
Wire Wire Line
	6150 1950 6150 1850
Wire Wire Line
	5650 1950 5650 1850
$Comp
L power:GND #PWR014
U 1 1 5EC2904B
P 6150 1950
F 0 "#PWR014" H 6150 1700 50  0001 C CNN
F 1 "GND" H 6155 1777 50  0000 C CNN
F 2 "" H 6150 1950 50  0001 C CNN
F 3 "" H 6150 1950 50  0001 C CNN
	1    6150 1950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR013
U 1 1 5EC285FC
P 5650 1950
F 0 "#PWR013" H 5650 1700 50  0001 C CNN
F 1 "GND" H 5655 1777 50  0000 C CNN
F 2 "" H 5650 1950 50  0001 C CNN
F 3 "" H 5650 1950 50  0001 C CNN
	1    5650 1950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 5EC24834
P 6150 1300
F 0 "R7" H 6220 1346 50  0000 L CNN
F 1 "103" H 6220 1255 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6080 1300 50  0001 C CNN
F 3 "~" H 6150 1300 50  0001 C CNN
	1    6150 1300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 5EC237DD
P 5650 1300
F 0 "R6" H 5720 1346 50  0000 L CNN
F 1 "103" H 5720 1255 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5580 1300 50  0001 C CNN
F 3 "~" H 5650 1300 50  0001 C CNN
	1    5650 1300
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D2
U 1 1 5EC22763
P 6150 1700
F 0 "D2" V 6189 1583 50  0000 R CNN
F 1 "LED" V 6098 1583 50  0000 R CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 6150 1700 50  0001 C CNN
F 3 "~" H 6150 1700 50  0001 C CNN
	1    6150 1700
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D1
U 1 1 5EC21A7A
P 5650 1700
F 0 "D1" V 5689 1583 50  0000 R CNN
F 1 "LED" V 5598 1583 50  0000 R CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 5650 1700 50  0001 C CNN
F 3 "~" H 5650 1700 50  0001 C CNN
	1    5650 1700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4600 1500 4600 1600
Connection ~ 4600 1500
Wire Wire Line
	4700 1500 4600 1500
Text GLabel 4700 1500 2    50   Input ~ 0
NRST
Wire Wire Line
	4600 2000 4600 1900
Wire Wire Line
	4600 1400 4600 1500
Wire Wire Line
	4600 1000 4600 1100
$Comp
L power:+3.3V #PWR011
U 1 1 5EC13B1C
P 4600 1000
F 0 "#PWR011" H 4600 850 50  0001 C CNN
F 1 "+3.3V" H 4615 1173 50  0000 C CNN
F 2 "" H 4600 1000 50  0001 C CNN
F 3 "" H 4600 1000 50  0001 C CNN
	1    4600 1000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR012
U 1 1 5EC13496
P 4600 2000
F 0 "#PWR012" H 4600 1750 50  0001 C CNN
F 1 "GND" H 4605 1827 50  0000 C CNN
F 2 "" H 4600 2000 50  0001 C CNN
F 3 "" H 4600 2000 50  0001 C CNN
	1    4600 2000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 5EC11D53
P 4600 1250
F 0 "R5" H 4670 1296 50  0000 L CNN
F 1 "103" H 4670 1205 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4530 1250 50  0001 C CNN
F 3 "~" H 4600 1250 50  0001 C CNN
	1    4600 1250
	1    0    0    -1  
$EndComp
$Comp
L Device:C C10
U 1 1 5EC117DC
P 4600 1750
F 0 "C10" H 4715 1796 50  0000 L CNN
F 1 "104" H 4715 1705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4638 1600 50  0001 C CNN
F 3 "~" H 4600 1750 50  0001 C CNN
	1    4600 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 3850 1200 3700
Connection ~ 1200 3850
Wire Wire Line
	1150 3850 1200 3850
$Comp
L power:GND #PWR010
U 1 1 5EBE6364
P 1150 3850
F 0 "#PWR010" H 1150 3600 50  0001 C CNN
F 1 "GND" V 1155 3722 50  0000 R CNN
F 2 "" H 1150 3850 50  0001 C CNN
F 3 "" H 1150 3850 50  0001 C CNN
	1    1150 3850
	0    1    1    0   
$EndComp
Wire Wire Line
	1200 4000 1200 3850
Wire Wire Line
	2050 3750 2050 3700
Wire Wire Line
	2100 3750 2050 3750
Wire Wire Line
	2050 3850 2050 4000
Wire Wire Line
	2100 3850 2050 3850
$Comp
L Device:C C5
U 1 1 5EBD3893
P 1350 4000
F 0 "C5" V 1602 4000 50  0000 C CNN
F 1 "20P" V 1511 4000 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 1388 3850 50  0001 C CNN
F 3 "~" H 1350 4000 50  0001 C CNN
	1    1350 4000
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C4
U 1 1 5EBD2635
P 1350 3700
F 0 "C4" V 1602 3700 50  0000 C CNN
F 1 "20P" V 1511 3700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 1388 3550 50  0001 C CNN
F 3 "~" H 1350 3700 50  0001 C CNN
	1    1350 3700
	0    -1   -1   0   
$EndComp
Text GLabel 2000 5650 0    50   Input ~ 0
LED1
Text GLabel 2000 5250 0    50   Input ~ 0
LED2
Wire Wire Line
	3400 5650 3500 5650
Wire Wire Line
	3400 5550 3500 5550
Wire Wire Line
	3400 5450 3500 5450
Wire Wire Line
	3500 5350 3400 5350
Text GLabel 3500 5650 2    50   Input ~ 0
USB_P
Text GLabel 3500 5550 2    50   Input ~ 0
USB_N
Text GLabel 3500 5450 2    50   Input ~ 0
RX
Text GLabel 3500 5350 2    50   Input ~ 0
TX
Wire Wire Line
	2000 3550 2100 3550
Text GLabel 2000 3550 0    50   Input ~ 0
BOOT0
Wire Wire Line
	2000 4650 2100 4650
Wire Wire Line
	1600 4650 1700 4650
$Comp
L power:GND #PWR05
U 1 1 5EBCA59E
P 1600 4650
F 0 "#PWR05" H 1600 4400 50  0001 C CNN
F 1 "GND" V 1605 4522 50  0000 R CNN
F 2 "" H 1600 4650 50  0001 C CNN
F 3 "" H 1600 4650 50  0001 C CNN
	1    1600 4650
	0    1    1    0   
$EndComp
$Comp
L Device:R R4
U 1 1 5EBC977E
P 1850 4650
F 0 "R4" V 1643 4650 50  0000 C CNN
F 1 "103" V 1734 4650 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1780 4650 50  0001 C CNN
F 3 "~" H 1850 4650 50  0001 C CNN
	1    1850 4650
	0    1    1    0   
$EndComp
Wire Wire Line
	2000 3350 2100 3350
Text GLabel 2000 3350 0    50   Input ~ 0
NRST
Wire Wire Line
	2800 3150 2800 3100
Wire Wire Line
	2900 3150 3000 3150
Connection ~ 2900 3150
Wire Wire Line
	2800 3150 2900 3150
Connection ~ 2800 3150
Wire Wire Line
	2700 3150 2800 3150
Connection ~ 2700 3150
Wire Wire Line
	2600 3150 2700 3150
$Comp
L power:+3.3V #PWR07
U 1 1 5EBC676A
P 2800 3100
F 0 "#PWR07" H 2800 2950 50  0001 C CNN
F 1 "+3.3V" H 2815 3273 50  0000 C CNN
F 2 "" H 2800 3100 50  0001 C CNN
F 3 "" H 2800 3100 50  0001 C CNN
	1    2800 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 6150 2700 6150
Connection ~ 2800 6150
Wire Wire Line
	2700 6150 2600 6150
Connection ~ 2700 6150
Wire Wire Line
	2600 6150 2450 6150
Connection ~ 2600 6150
Wire Wire Line
	2900 6150 2800 6150
$Comp
L power:GND #PWR06
U 1 1 5EBC4F36
P 2450 6150
F 0 "#PWR06" H 2450 5900 50  0001 C CNN
F 1 "GND" V 2455 6022 50  0000 R CNN
F 2 "" H 2450 6150 50  0001 C CNN
F 3 "" H 2450 6150 50  0001 C CNN
	1    2450 6150
	0    1    1    0   
$EndComp
Wire Wire Line
	3400 5050 3500 5050
Wire Wire Line
	3400 4850 3500 4850
Wire Wire Line
	3400 4650 3500 4650
Text GLabel 3500 5050 2    50   Input ~ 0
RST
Text GLabel 3500 4850 2    50   Input ~ 0
TCK
Text GLabel 3500 4650 2    50   Input ~ 0
TMS
Wire Notes Line
	4200 2700 4200 450 
Connection ~ 3900 900 
Wire Wire Line
	3900 800  3900 900 
$Comp
L power:+3.3V #PWR04
U 1 1 5EBBC65A
P 3900 800
F 0 "#PWR04" H 3900 650 50  0001 C CNN
F 1 "+3.3V" H 3915 973 50  0000 C CNN
F 2 "" H 3900 800 50  0001 C CNN
F 3 "" H 3900 800 50  0001 C CNN
	1    3900 800 
	1    0    0    -1  
$EndComp
Connection ~ 3550 1350
Wire Wire Line
	3900 1350 3550 1350
Wire Wire Line
	3900 1200 3900 1350
Wire Wire Line
	3500 900  3900 900 
Wire Wire Line
	3550 1000 3500 1000
Wire Wire Line
	3550 1050 3550 1000
Wire Wire Line
	3200 1350 3550 1350
Connection ~ 3200 1350
Wire Wire Line
	3200 1350 3200 1300
Wire Wire Line
	2650 1350 2650 1300
Wire Wire Line
	3200 1350 2650 1350
$Comp
L power:GND #PWR03
U 1 1 5EBBA6AC
P 3200 1350
F 0 "#PWR03" H 3200 1100 50  0001 C CNN
F 1 "GND" H 3205 1177 50  0000 C CNN
F 2 "" H 3200 1350 50  0001 C CNN
F 3 "" H 3200 1350 50  0001 C CNN
	1    3200 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 900  2900 900 
Wire Wire Line
	2650 1000 2650 900 
Connection ~ 2900 900 
Wire Wire Line
	2900 1000 2900 900 
$Comp
L Device:C C2
U 1 1 5EBB61AD
P 3550 1200
F 0 "C2" H 3665 1246 50  0000 L CNN
F 1 "103" H 3665 1155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3588 1050 50  0001 C CNN
F 3 "~" H 3550 1200 50  0001 C CNN
	1    3550 1200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5EBB5F88
P 3900 1050
F 0 "C3" H 4015 1096 50  0000 L CNN
F 1 "106" H 4015 1005 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 3938 900 50  0001 C CNN
F 3 "~" H 3900 1050 50  0001 C CNN
	1    3900 1050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5EBB4F22
P 2650 1150
F 0 "C1" H 2765 1196 50  0000 L CNN
F 1 "106" H 2765 1105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2688 1000 50  0001 C CNN
F 3 "~" H 2650 1150 50  0001 C CNN
	1    2650 1150
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:SPX3819M5-L-3-3 U2
U 1 1 5EBB3A7C
P 3200 1000
F 0 "U2" H 3200 1342 50  0000 C CNN
F 1 "SPX3819M5-L-3-3" H 3200 1251 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 3200 1325 50  0001 C CNN
F 3 "https://www.exar.com/content/document.ashx?id=22106&languageid=1033&type=Datasheet&partnumber=SPX3819&filename=SPX3819.pdf&part=SPX3819" H 3200 1000 50  0001 C CNN
	1    3200 1000
	1    0    0    -1  
$EndComp
$Comp
L MCU_ST_STM32F1:STM32F103C8Tx U1
U 1 1 5EBA39FD
P 2800 4650
F 0 "U1" H 2750 3061 50  0000 C CNN
F 1 "STM32F103C8Tx" H 2750 2970 50  0000 C CNN
F 2 "Package_QFP:LQFP-48_7x7mm_P0.5mm" H 2200 3250 50  0001 R CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/CD00161566.pdf" H 2800 4650 50  0001 C CNN
	1    2800 4650
	1    0    0    -1  
$EndComp
$EndSCHEMATC
